namespace Estagio.Escola.AcessoDados.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CidadeEstadoBairro : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BAIRRO",
                c => new
                    {
                        ID_BAIRRO = c.Int(nullable: false, identity: true),
                        NOME_BAIRRO = c.String(nullable: false, maxLength: 100),
                        RUA_ENDERECO = c.String(nullable: false, maxLength: 100),
                        NUMERO = c.String(nullable: false, maxLength: 10),
                        CidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID_BAIRRO)
                .ForeignKey("dbo.CIDADES", t => t.CidadeId, cascadeDelete: true)
                .Index(t => t.CidadeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BAIRRO", "CidadeId", "dbo.CIDADES");
            DropIndex("dbo.BAIRRO", new[] { "CidadeId" });
            DropTable("dbo.BAIRRO");
        }
    }
}
