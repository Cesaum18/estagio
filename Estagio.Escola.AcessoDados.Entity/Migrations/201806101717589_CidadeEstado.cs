namespace Estagio.Escola.AcessoDados.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CidadeEstado : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CIDADES",
                c => new
                    {
                        ID_CIDADE = c.Int(nullable: false, identity: true),
                        NOME_CIDADE = c.String(nullable: false, maxLength: 100),
                        EstadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID_CIDADE)
                .ForeignKey("dbo.ESTADOS", t => t.EstadoId, cascadeDelete: true)
                .Index(t => t.EstadoId);
            
            CreateTable(
                "dbo.ESTADOS",
                c => new
                    {
                        ESTADO_ID = c.Int(nullable: false, identity: true),
                        NOME_ESTADO = c.String(nullable: false, maxLength: 100),
                        UF = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.ESTADO_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CIDADES", "EstadoId", "dbo.ESTADOS");
            DropIndex("dbo.CIDADES", new[] { "EstadoId" });
            DropTable("dbo.ESTADOS");
            DropTable("dbo.CIDADES");
        }
    }
}
