﻿using Escola.Comum.Entity;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.TypeConfiguration
{
    class BairroTypeConfiguration : EstagioEntityAbstractConfig<BairroBase>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.BairroId)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("ID_BAIRRO");

            Property(p => p.NomeBairro)
                .IsRequired()
                .HasColumnName("NOME_BAIRRO")
                .HasMaxLength(100);

            Property(p => p.Rua)
                .IsRequired()
                .HasColumnName("RUA_ENDERECO")
                .HasMaxLength(100);

            Property(p => p.Numero)
                .IsRequired()
                .HasColumnName("NUMERO")
                .HasMaxLength(10);


        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.BairroId);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
                  HasRequired(p => p.CidadeBase)
                  .WithMany(p => p.BairrosBase)
                  .HasForeignKey(p => p.CidadeId);


        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("BAIRRO");
        }
    }
}
