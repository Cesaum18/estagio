﻿using Escola.Comum.Entity;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.TypeConfiguration
{
    class EstadoTypeConfiguration : EstagioEntityAbstractConfig<EstadoBase>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.EstadoId)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("ESTADO_ID");
                


            Property(p => p.NomeEstado)
                .IsRequired()
                .HasColumnName("NOME_ESTADO")
                .HasMaxLength(100);

            Property(p => p.UF)
                .IsRequired()
                .HasColumnName("UF")
                .HasMaxLength(2);
            
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.EstadoId);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {

            HasMany(p => p.CidadeBases)
              .WithRequired(p => p.EstadoBase)
              .HasForeignKey(p => p.EstadoId);


        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("ESTADOS");
        }
    }
}
