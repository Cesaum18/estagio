﻿using Escola.Comum.Entity;
using Estagio.Escola.Dominio.Cadastro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.TypeConfiguration
{
    class PessoaBaseTypeConfiguration : EstagioEntityAbstractConfig<PessoaBase>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.PessoaId)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("ID_PESSOA");

            Property(p => p.Nome)
                .IsRequired()
                .HasColumnName("NOME_PESSOA")
                .HasMaxLength(50);

            Property(p => p.CPF)
                .IsRequired()
                .HasColumnName("CPF_PESSOA")
                .HasMaxLength(20);

            Property(p => p.CNPJ)
                .IsOptional()
                .HasColumnName("CNPJ_PESSOA")
                .HasMaxLength(20);

            Property(p => p.Telefone)
                .IsRequired()
                .HasColumnName("TELEFONE")
                .HasMaxLength(10);

            Property(p => p.DataNascimento)
                .IsRequired()
                .HasColumnName("DT_NASCIMENTO")
                .HasColumnType("Date");

            Property(p => p.Sexo)
                .IsRequired()
                .HasColumnName("SEXO");
        

        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(p => p.PessoaId);
            
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
           
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("PESSOA_BASE");
        }
    }
}
