﻿using Escola.Comum.Entity;
using Estagio.Escola.Dominio.Cadastro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.TypeConfiguration
{
    class PessoaTypeConfiguration : EstagioEntityAbstractConfig<PessoaBase>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Id)
            .IsRequired()
            .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
            .HasColumnType("PE_ID");
        }

        protected override void ConfigurarChavePrimaria()
        {
            throw new NotImplementedException();
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
            throw new NotImplementedException();
        }

        protected override void ConfigurarNomeTabela()
        {
            throw new NotImplementedException();
        }
    }
}
