﻿using Escola.Comum.Entity;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.TypeConfiguration
{
    class CidadeTypeConfiguration : EstagioEntityAbstractConfig<CidadeBase>
    {
        
        protected override void ConfigurarCamposTabela()
        {

            Property(p => p.CidadeId)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("ID_CIDADE");

            Property(p => p.NomeCidade)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnName("NOME_CIDADE");
        }

        protected override void ConfigurarChavePrimaria()
        {
            HasKey(pk => pk.CidadeId);
        }

        protected override void ConfigurarChavesEstrangeiras()
        {
              HasRequired(p => p.EstadoBase)
              .WithMany(p => p.CidadeBases)
              .HasForeignKey(p => p.EstadoId);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("CIDADES");
        }
    }
}
