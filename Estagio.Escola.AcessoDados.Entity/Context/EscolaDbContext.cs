﻿using Estagio.Escola.AcessoDados.Entity.TypeConfiguration;
using Estagio.Escola.Dominio.Cadastro;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.AcessoDados.Entity.Context
{
    public class EscolaDbContext : DbContext
    {
        public DbSet<EstadoBase> EstadosBase { get; set; }
        public DbSet<CidadeBase> CidadesBase { get; set; }
        public DbSet<BairroBase> BairrosBase { get; set; }
        public DbSet<PessoaBase> Aluno { get; set; }

        public EscolaDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EstadoTypeConfiguration());

            modelBuilder.Configurations.Add(new CidadeTypeConfiguration());

            modelBuilder.Configurations.Add(new  BairroTypeConfiguration());
        }

    }
}