﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Estagio.Escola.AcessoDados.Entity.Context;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;

namespace Estagio.Escola.Web.Controllers
{
    public class BairroController : Controller
    {
        private EscolaDbContext db = new EscolaDbContext();

        // GET: Bairro
        public ActionResult Index()
        {
            var bairrosBase = db.BairrosBase.Include(b => b.CidadeBase);
            return View(bairrosBase.ToList());
        }

        // GET: Bairro/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BairroBase bairroBase = db.BairrosBase.Find(id);
            if (bairroBase == null)
            {
                return HttpNotFound();
            }
            return View(bairroBase);
        }

        // GET: Bairro/Create
        public ActionResult Create()
        {
            ViewBag.CidadeId = new SelectList(db.CidadesBase, "CidadeId", "NomeCidade");
            return View();
        }

        // POST: Bairro/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BairroId,NomeBairro,Rua,Numero,CidadeId")] BairroBase bairroBase)
        {
            if (ModelState.IsValid)
            {
                db.BairrosBase.Add(bairroBase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CidadeId = new SelectList(db.CidadesBase, "CidadeId", "NomeCidade", bairroBase.CidadeId);
            return View(bairroBase);
        }

        // GET: Bairro/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BairroBase bairroBase = db.BairrosBase.Find(id);
            if (bairroBase == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeId = new SelectList(db.CidadesBase, "CidadeId", "NomeCidade", bairroBase.CidadeId);
            return View(bairroBase);
        }

        // POST: Bairro/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BairroId,NomeBairro,Rua,Numero,CidadeId")] BairroBase bairroBase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bairroBase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CidadeId = new SelectList(db.CidadesBase, "CidadeId", "NomeCidade", bairroBase.CidadeId);
            return View(bairroBase);
        }

        // GET: Bairro/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BairroBase bairroBase = db.BairrosBase.Find(id);
            if (bairroBase == null)
            {
                return HttpNotFound();
            }
            return View(bairroBase);
        }

        // POST: Bairro/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BairroBase bairroBase = db.BairrosBase.Find(id);
            db.BairrosBase.Remove(bairroBase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
