﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Estagio.Escola.AcessoDados.Entity.Context;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;

namespace Estagio.Escola.Web.Controllers
{
    public class EstadoController : Controller
    {
        private EscolaDbContext db = new EscolaDbContext();

        // GET: Estado
        public ActionResult Index()
        {
            return View(db.EstadosBase.ToList());
        }

        // GET: Estado/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoBase estadoBase = db.EstadosBase.Find(id);
            if (estadoBase == null)
            {
                return HttpNotFound();
            }
            return View(estadoBase);
        }

        // GET: Estado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Estado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EstadoId,NomeEstado,UF")] EstadoBase estadoBase)
        {
            if (ModelState.IsValid)
            {
                db.EstadosBase.Add(estadoBase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadoBase);
        }

        // GET: Estado/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoBase estadoBase = db.EstadosBase.Find(id);
            if (estadoBase == null)
            {
                return HttpNotFound();
            }
            return View(estadoBase);
        }

        // POST: Estado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EstadoId,NomeEstado,UF")] EstadoBase estadoBase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadoBase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadoBase);
        }

        // GET: Estado/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoBase estadoBase = db.EstadosBase.Find(id);
            if (estadoBase == null)
            {
                return HttpNotFound();
            }
            return View(estadoBase);
        }

        // POST: Estado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoBase estadoBase = db.EstadosBase.Find(id);
            db.EstadosBase.Remove(estadoBase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
