﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Estagio.Escola.AcessoDados.Entity.Context;
using Estagio.Escola.Dominio.Cadastro.CidadeEstado;

namespace Estagio.Escola.Web.Controllers
{
    public class CidadeController : Controller
    {
        private EscolaDbContext db = new EscolaDbContext();

        // GET: Cidade
        public ActionResult Index()
        {
            var cidadesBase = db.CidadesBase.Include(c => c.EstadoBase);
            return View(cidadesBase.ToList());
        }

        // GET: Cidade/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CidadeBase cidadeBase = db.CidadesBase.Find(id);
            if (cidadeBase == null)
            {
                return HttpNotFound();
            }
            return View(cidadeBase);
        }

        // GET: Cidade/Create
        public ActionResult Create()
        {
            ViewBag.EstadoId = new SelectList(db.EstadosBase, "EstadoId", "NomeEstado");
            return View();
        }

        // POST: Cidade/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CidadeId,NomeCidade,EstadoId")] CidadeBase cidadeBase)
        {
            if (ModelState.IsValid)
            {
                db.CidadesBase.Add(cidadeBase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoId = new SelectList(db.EstadosBase, "EstadoId", "NomeEstado", cidadeBase.EstadoId);
            return View(cidadeBase);
        }

        // GET: Cidade/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CidadeBase cidadeBase = db.CidadesBase.Find(id);
            if (cidadeBase == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoId = new SelectList(db.EstadosBase, "EstadoId", "NomeEstado", cidadeBase.EstadoId);
            return View(cidadeBase);
        }

        // POST: Cidade/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CidadeId,NomeCidade,EstadoId")] CidadeBase cidadeBase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cidadeBase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoId = new SelectList(db.EstadosBase, "EstadoId", "NomeEstado", cidadeBase.EstadoId);
            return View(cidadeBase);
        }

        // GET: Cidade/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CidadeBase cidadeBase = db.CidadesBase.Find(id);
            if (cidadeBase == null)
            {
                return HttpNotFound();
            }
            return View(cidadeBase);
        }

        // POST: Cidade/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CidadeBase cidadeBase = db.CidadesBase.Find(id);
            db.CidadesBase.Remove(cidadeBase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
