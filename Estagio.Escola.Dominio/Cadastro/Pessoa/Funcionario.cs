﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.Dominio.Cadastro.Pessoa
{
    class Funcionario : PessoaBase
    {
        
        public string Funcao { get; set; }

        public virtual ICollection<PessoaBase> Pessoas { get; set; }
    }
}
