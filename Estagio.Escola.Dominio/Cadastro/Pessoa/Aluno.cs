﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Estagio.Escola.Dominio.Cadastro.Pessoa

{
    public class Aluno : PessoaBase
    {
        public string NomeMae { get; set; }
        public string CpfMae { get; set; }
        public string RgMae { get; set; }
        public DateTime DtNascMae { get; set; }
        public string CelMae { get; set; }

        public string NomePai { get; set; }
        public string CpfPai { get; set; }
        public string RgPai { get; set; }
        public DateTime DtNascPai { get; set; }
        public string CelPai { get; set; }

        public int Serie { get; set; }
        public string Periodo { get; set; }
        public string Observacoes { get; set; }

        public virtual ICollection<PessoaBase> Pessoas { get; set; }





    }
}
