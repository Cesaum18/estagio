﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.Dominio.Cadastro.Contrato
{
    class ContratoMatricula
    {
        public int Id { get; set; }

        public DateTime DtInicioContrato { get; set; }
        public DateTime DtTerminoContrato { get; set; }
        public int ValorContrato { get; set; }

    }
}
