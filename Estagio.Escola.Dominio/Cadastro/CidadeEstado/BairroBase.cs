﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.Dominio.Cadastro.CidadeEstado
{
    public class BairroBase
    {
        public int BairroId { get; set; }

        public string NomeBairro { get; set; }
        public string Rua { get; set; }
        public string Numero { get; set; }

        public virtual CidadeBase CidadeBase { get; set; }
        public int CidadeId { get; set; }
        
    }
}
