﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.Dominio.Cadastro.CidadeEstado
{
    public class EstadoBase
    {
        public int EstadoId { get; set; }

        public string NomeEstado { get; set; }
        public string UF { get; set; }
        

        public List<CidadeBase> CidadeBases { get; set; }

    }
}
