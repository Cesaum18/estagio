﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estagio.Escola.Dominio.Cadastro.CidadeEstado
{
    public class CidadeBase
    {
        public int CidadeId { get; set; }
        
        public string NomeCidade { get; set; }

        public virtual EstadoBase EstadoBase { get; set; }
        public int EstadoId { get; set; }
        
       public List<BairroBase> BairrosBase { get; set; }


    }
}
